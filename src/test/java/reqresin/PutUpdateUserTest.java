package reqresin;

import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class PutUpdateUserTest {
    @Test
    @DisplayName("Вызов метода PUT /updateUser. Обновление пользователя")
    public void successPutUpdateUser() throws IOException, URISyntaxException, InterruptedException {
        byte[] out = "{\"name\":\"morpheus\",\"job\":\"zion resident\"}".getBytes(StandardCharsets.UTF_8);

        HttpRequest request = HttpRequest.newBuilder()
                .uri(new URI("https://reqres.in/api/users/2"))
                .PUT(HttpRequest.BodyPublishers.ofByteArray(out))
                .build();

        HttpResponse response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        System.out.println(response.body());

        assertEquals(200, response.statusCode(), "status code не соответствует");
    }
}
